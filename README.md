# Tutorials, introductions, and other stuff

## Git
- [Atlassian tutorial](https://www.atlassian.com/git/tutorials/)
- [PDF](https://services.github.com/kit/downloads/github-git-cheat-sheet.pdf) and [interactive](http://ndpsoftware.com/git-cheatsheet.html) cheat sheet
- [Copy of the book "Pro Git" by Scott Chacon and Ben Straub](https://git-scm.com/book/en/v2)

Convert existing directory to a Git repository.

```
$ git init
```

Create a new folder and initialize it as a Git repository.

```
# git init <folder-name>
```

Create a new folder and initialize it as a Git repository, omitting the working directory. This means that you won't be able to edit and make changes in that repository. Generally, the central repositories (main project page) are created using the `--bare` flag, and serve more as a storage area for other repos.

```
$ git init --bare <folder-name>
```

Add changes to staging area.

```
$ git add <file>
```

Track staged and unstaged files.

```
$ git status
```

Commit changes. This only changes your local copy, not the remote.

```
$ git commit -m "message"
```

Push changes to remote.

```
$ git push <remote> <branch>
```

List local branches.

```
$ git branch
* master
```

List remote branches.

```
$ git branch -r
origin/master
```

List all branches.

```
$ git branch -a
* master
origin/master
```

Create a new local branch which tracks a particular remote branch.

```
$ git branch --track <new-branch-name> <remote-branch-name-to-track>
```

Update files in the current working directory to the version in the specified branch.

```
$ git checkout <branch>
```

Checkout a previously committed version of file.

```
$ git checkout <commit> <file>
```

View all commits.

```
$ git log
```

Revert back to a previous commit. This creates a new commit to undo changes in the given commit.

```
$ git revert <commit>
```


## Working with MATLAB on Jupyter notebook

First, install [MATLAB engine API for Python](https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html). Then, install the following packages.

```
$ sudo pip install pymatbridge --user
$ sudo pip install matlab_kernel --user
$ sudo pip install ipywidgets --user
```

```
$ python -m matlab_kernel install
```

Make sure your `PATH` variable can locate ipython. Also, save the following in your bash profile.

```
export MATLAB_EXECUTABLE=/Applications/MATLAB_R2016b.app/bin/matlab
```

Then, run iPython in your browser and select MATLAB.

```
$ ipython notebook
```