# Plotting in Python

We'll be using `[matplotlib](http://matplotlib.org/)` and `[seaborn](http://seaborn.pydata.org/)` to create plots in Python.

### Installation

1. `jupyter` or `ipython`
2. `matplotlib`
3. `seaborn`

If you're using MacOS, you would need to follow the steps listed [here](http://blog.olgabotvinnik.com/blog/2012/11/15/2012-11-15-how-to-set-helvetica-as-the-default-sans-serif-font-in/) to be able to use Helvetica as your figure text font.


